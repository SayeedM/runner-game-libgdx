package com.io.runner;


import com.badlogic.gdx.Game;
import com.io.runner.screens.GameScreen;
import com.io.runner.utils.AssetsManager;
import com.io.runner.utils.GameEventListener;
import com.io.runner.utils.GameManager;


public class RunnerGame extends Game {

	public RunnerGame(GameEventListener listener) {
		GameManager.getInstance().setGameEventListener(listener);
	}

	@Override
	public void create () {
		AssetsManager.loadAssets();

		setScreen(new GameScreen());
	}

	@Override
	public void dispose() {
		super.dispose();
		AssetsManager.dispose();
	}

}
