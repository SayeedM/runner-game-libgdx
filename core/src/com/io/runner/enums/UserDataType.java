package com.io.runner.enums;

/**
 * Created by SayeedM on 3/9/2016.
 */
public enum UserDataType {
    GROUND,
    RUNNER,
    ENEMY
}
