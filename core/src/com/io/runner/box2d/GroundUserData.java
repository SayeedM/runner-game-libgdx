package com.io.runner.box2d;

import com.io.runner.enums.UserDataType;

/**
 * Created by SayeedM on 3/9/2016.
 */
public class GroundUserData extends UserData {

    public GroundUserData(float width, float height){
        super(width, height);
        userDataType = UserDataType.GROUND;
    }




}
