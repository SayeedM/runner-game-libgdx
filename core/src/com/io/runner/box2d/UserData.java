package com.io.runner.box2d;

import com.io.runner.enums.UserDataType;

/**
 * Created by SayeedM on 3/9/2016.
 */
public abstract class UserData {
    protected UserDataType userDataType;

    protected float width;
    protected float height;



    public UserData() {

    }

    public UserDataType getUserDataType() {
        return userDataType;
    }

    public UserData(float width, float height) {
        this.width = width;
        this.height = height;
    }


    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }




}
